
# Changelog for VRE Definition Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v5.3.0]

- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]

## [v5.2.0] - 2021-07-13

- VRE-Definition portlet porting to git and remove HomeLibrary Dependency [#21804]

## [v5.0.0] - 2016-05-21

- Completely redesigned and reimplemented from scratch, modern look and feel
